@component('mail::message')
# Introduction

Welcome instagram, **thnk you** _so much_

@component('mail::button', ['url' => 'http://127.0.0.1:8000/post','color'=>'success'])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
