@extends('layouts.master')
@section('content')
    <a type="submit" href="{{route('post.index')}}" class="btn btn-primary mb-5">Back</a>

    <div class="card-columns">
        @foreach($posts as $post)
            <div class="card " style="width: 300px">
                {{--                    @foreach($users as $userss)--}}
                {{--                        @if($userss->id==$post->user_id)--}}
                <img src="{{url('/storage/'.$post->user->profile_image)}}" style="width: 60px;"
                     class="rounded-circle img-thumbnail">
                <a class="card-text">Posted By : {{$post->user->name}}</a>
                {{--                        @endif--}}
                {{--                    @endforeach--}}
                <hr>
                <img class="card-img-top" src="{{url('/storage/'.$post->post_url)}}">
                <div class="card-body">
                    <div class="interaction">
                        @if(sizeof($post->users)  == null)
                            <a href="{{url("/like/{$post->id}")}}" class="btn btn-sm btn-success mb-2">like</a>
                        @else
                            <a href="{{url("/like/{$post->id}")}}" class="btn btn-sm btn-danger mb-2">Unlike</a>
                        @endif
                        <form method="post" action="{{url("/comment/{$post->id}")}}" class="form-group">
                            @csrf
                            @method('get')
                            <input type="text" class="form-control" name="comment" id="comment"
                                   placeholder="Comment...">
                            <input type="submit" class="form-control" name="submit" value="submit">
                        </form>
                        @foreach($post->comments as $comment)
                            {{--                                @if($post->id==$comment->post_id)--}}
                            {{--                                        @foreach($users as $userss)--}}
                            {{--                                            @if($userss->id==$comment->user_id)--}}
                            <img src="{{url('/storage/'.$comment->user->profile_image)}}" style="width: 45px;"
                                 class="rounded-circle img-thumbnail">
                            <small><a class="card-text">{{$comment->user->name}}<br></a></small>
                            {{--                                            @endif--}}
                            {{--                                        @endforeach--}}
                            <small>{{$comment->comment}}<br></small>
                            <hr>
                            {{--                                @endif--}}
                        @endforeach
                        @if(Auth::user()==$post->user)
                            <form action="/post/{{$post->id}}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger form-control">Delete</button>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
