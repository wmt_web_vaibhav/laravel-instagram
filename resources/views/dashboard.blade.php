@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="offset-lg-4 col-lg-4 col-sm-6 col-12 main-section text-center">
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-12 profile-header"></div>
                </div>
                <div class="row user-detail">
                    <div class="col-lg-12 col-sm-12 col-12">
                        <img src="{{url('/storage/'.$user->profile_image)}}" class="rounded-circle img-thumbnail">
                        <h5>{{$user->name}}</h5>
                        <h5>{{$user->username}}</h5>
                        <p>{{$user->dob}}</p>
{{--                        <p>{{dd(Url::current())}}</p>--}}
                        <hr>
                        <a class="btn btn-outline-success form-control" href="{{ route('post.create') }}">Add post</a>
                        <hr>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle form-control" type="button"
                                    id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                Country
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                @foreach($country as $country)
                                    <form method="post" class="form" action="{{url("/countrywpost/{$country->id}")}}">
                                        @csrf
                                        @method('get')
                                        <button class="btn btn-outline-primary form-control" type="submit"
                                                value="{{ $country->id }}">{{ $country->name }}</button>
                                    </form>
                                @endforeach
                            </div>
                        </div>
                        <hr>
                        <form method="post" class="form" action="{{route('onlyimage')}}">
                            @csrf
                            @method('get')
                            <button class="btn btn-outline-dark form-control" type="submit" value="">Image</button>
                        </form>
                        <hr>
                        <form method="post" class="form" action="{{route('onlyvideo')}}">
                            @csrf
                            @method('get')
                            <button class="btn btn-outline-warning form-control" type="submit" value="">Videos</button>
                        </form>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-columns py-3">
        @foreach($posts as $post)
            <div class="card mx-2 " style="width: 350px">
                {{--                @foreach($users as $userss)--}}
                {{--                    @if($userss->id==$post->user_id)--}}
                <img src="{{url('/storage/'.$post->user->profile_image)}}" style="width: 60px;"
                     class="rounded-circle img-thumbnail">
                <a class="card-text">Posted By : {{$post->user->name}}</a>
                {{--                    @endif--}}
                {{--                @endforeach--}}
                <hr>
                <img class="card-img-top" src="{{url('/storage/'.$post->post_url)}}">
                {{--                <a class="card-text">{{$posts->user->user_id}}</a>--}}
                <div class="card-body">
                    <div class="interaction">
                        @if(sizeof($post->users)  == null)
                            {{--                            {{dd('unlike',$post->like)}}--}}
                            <a href="{{url("/like/{$post->id}")}}" class="btn btn-sm btn-success mb-2">like</a>
                        @else
                            <a href="{{url("/like/{$post->id}")}}" class="btn btn-sm btn-danger mb-2">Unlike</a>
                            {{--                            {{dd('like',$post->like)}}--}}
                        @endif
                        <form method="post" action="{{url("/comment/{$post->id}")}}" class="form-group">
                            @csrf
                            @method('get')
                            <input type="text" class="form-control" name="comment" id="comment"
                                   placeholder="Comment...">
                            <input type="submit" class="form-control" name="submit" value="submit">
                        </form>
                        @foreach($post->comments as $comment)
                            {{--                            {{dd($post->user->profile_image)}}--}}
                            {{--                            @if($post->id==$comment->post_id)--}}
                            {{--                                    @foreach($users as $userss)--}}
                            {{--                                        @if($userss->id==$comment->user_id)--}}
                            <img src="{{url('/storage/'.$comment->user->profile_image)}}" style="width: 45px;"
                                 class="rounded-circle img-thumbnail">
                            <small><a class="card-text">{{$comment->user->name}}<br></a></small>
                            {{--                                        @endif--}}
                            {{--                                    @endforeach--}}
                            <small>{{$comment->comment}}<br></small>
                            <hr>
                            {{--                            @endif--}}
                        @endforeach
                        @if(Auth::id()==$post->user_id)
                            <form action="/post/{{$post->id}}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger form-control">Delete</button>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
