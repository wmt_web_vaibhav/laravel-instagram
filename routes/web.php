<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Jobs\sndmailjob;
use Carbon\Carbon;
use App\User;


Route::get('/', function () {
    return view('welcome');
});

    Route::resource('home','UserController');
    Route::resource('post','PostController');
    Route::get('login','UserController@login')->name('login');
    Route::post('login','UserController@login')->name('login');
    Route::get('/logout','UserController@logout')->name('logout');
    Route::get('comment/{id}','PostController@comment')->name('comment');
    Route::get('countrywpost/{id}','PostController@countrywpost')->name('countrywpost');
    Route::get('onlyimage/','PostController@onlyimage')->name('onlyimage');
    Route::get('onlyvideo/','PostController@onlyvideo')->name('onlyvideo');
    Route::get('like/{id}','PostController@like')->name('like');
    Route::get('unlike/{id}','PostController@unlike')->name('unlike');
