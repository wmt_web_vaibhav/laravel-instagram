<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/', function () {
    return view('welcome');
});

Route::resource('home','UserController');
Route::resource('post','PostController');
Route::get('login','UserController@login')->name('login');
Route::post('login','UserController@login')->name('login');
Route::get('/logout','UserController@logout')->name('logout');
Route::get('comment/{id}','PostController@comment')->name('comment');
Route::get('countrywpost/{id}','PostController@countrywpost')->name('countrywpost');
Route::get('onlyimage/','PostController@onlyimage')->name('onlyimage');
Route::get('onlyvideo/','PostController@onlyvideo')->name('onlyvideo');
Route::get('like/{id}','PostController@like')->name('like');


Route::get('data','UserController@data')->name('data');
