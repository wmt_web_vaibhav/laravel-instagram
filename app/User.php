<?php

namespace App;


use App\Events\AddEvent;
use App\Observers\UserObserver;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements Authenticatable
{
    use \Illuminate\Auth\Authenticatable;

    protected $guarded=[];

//    protected $dispatchesEvents = [
//        'created' => AddEvent::class,
////        'updated' => UserObserver::class,
////        'deleted' => UserObserver::class,
//    ];

    public function getProfileImageAttributes($value){

        return url('/storage/'.$value);
    }
    public function posts(){
        return $this->hasMany(Post::class);
    }
    public function post()
    {
        return $this->belongsToMany(Post::class,'post_user')->withPivot('post_id','user_id');
    }




}
