<?php

namespace App;
use App\User;
use App\Comment;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $guarded=[];


    public function posts(){
        return $this->hasManyThrough(Post::class,User::class,'country_id','user_id','id','id');
    }

}
