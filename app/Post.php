<?php

namespace App;
use App\Country;
use App\Like;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{
    public $timestamps = false;

    protected $guarded=[];

    public function getProfileImageAttributes($value){

        return url('/storage/'.$value);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function comments(){
        return $this->hasMany(Comment::class);
    }
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

//    public function like(){
//        return $this->belongsToMany(User::class)->where('user_id',Auth::id());
//    }
}
