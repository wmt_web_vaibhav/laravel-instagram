<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Country;
use App\Events\AddEvent;
use App\Http\Requests\postvalidate;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user=Auth::user();
        $country=Country::all();
        $posts=Post::with(['users' => function($id){
            $id->where('user_id',Auth::id());
        } ])->orderBy('created_at','desc')->get();

//        dd($posts) ;

        return view('dashboard',compact('posts','user','country'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addpost');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(postvalidate $request)
    {

        $request->validated();
        $image_type = ['jpg', 'jpeg', 'png', 'gif'];
        $image = $request->file('post_url');
        $originalname = uniqid('Img',10) .'.'.$image->getClientOriginalExtension();
        $extension = $image->getClientOriginalExtension();
        $path=in_array($extension,$image_type)?$image->storeAs('/images/userimagepost',$originalname,'public'): $image->storeAs('/video/uservideopost',$originalname,'public');
        $post=Post::create([
            'postable_type'=>in_array($extension,$image_type) ? 'image':'video',
            'post_url' => $path,
            'title'=>$request->title,
            'user_id'=>Auth::id(),
        ]);
        $request->user()->posts()->save($post);
        return redirect()->route('post.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        if(Auth::user()!=$post->user){
            return redirect()->back();
        }
        $post->delete();
        return redirect()->route('post.index');
    }

    public function comment(Request $request,$post_id)
    {
        $comment=Comment::create([
            'comment'=>$request->comment,
        'user_id'=>Auth::id(),
        'post_id'=>$post_id,
        ]);
        $comment->save();

        return redirect()->route('post.index');
    }

    public function countrywpost($id)
    {
        $posts=Country::find($id)->posts;
        return view('countrywpost',compact('posts'));
    }

    public function onlyimage()
    {
        $user=Auth::user();
        $country=Country::all();
        $posts=Post::with(['users'=> function($id){
        $id->where('user_id',Auth::id());
    } ])->where('postable_type','=','image')->orderBy('created_at','desc')->get();
        return view('countrywpost',compact('posts','user','country'));
    }

    public function onlyvideo()
    {
        $user=Auth::user();
        $country=Country::all();
        $posts=Post::with(['users'=> function($id){
            $id->where('user_id',Auth::id());
        } ])->where('postable_type','=','video')->orderBy('created_at','desc')->get();
        return view('countrywpost',compact('posts','user','country'));
    }

    public function like($id){
        $post=Post::with('users')->find($id);
        $post->users()->toggle(Auth::id());
        return redirect()->route('post.index');
    }
//    public function unlike($id){
//        $post=Post::with('users')->find($id);
//        $post->users()->detach(Auth::id());
//        return redirect()->route('post.index');
//    }

}
