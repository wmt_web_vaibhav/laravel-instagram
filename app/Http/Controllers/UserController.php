<?php

namespace App\Http\Controllers;

use App\Events\AddEvent;
use App\Jobs\sndmailjob;
use App\Post;
use Carbon\Carbon;
use App\Country;
use App\Http\Requests\Updateuservalidate;
use App\Http\Requests\Uservalidate;
use App\Mail\sndmail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use PharIo\Manifest\Email;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $country=Country::all();
        return view('home',compact('country'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Uservalidate $request)
    {
//        $request->validated();
//        $image = $request->file('profile_image');
//        $originalname = $image->getClientOriginalName();
//        $path = $image->storeAs('/images/profileimages',$originalname,'public');
//
//        $user = User::create([
//            'name' => $request->name,
//        'username'=>$request->username,
//        'email' => $request->email,
//        'password' => bcrypt($request->password),
//        'dob'=>$request->date,
//        'country_id'=>$request->country,
//        'profile_image' => $path,
//
//        ]);
//        $user->save();
////        event(new AddEvent($user));
//        Auth::login($user);
//        return redirect()->route('post.index');
        $student = new User();
        $student->name = $request->name;
        $student->username=$request->username;
        $student->email=$request->email;
        $student->password=$request->password;
        $student->dob=$request->dob;
        $student->country_id=$request->country;
        $student->profile_image=$request->profile_image;
        $student->save();
        return response()->json([
            "message" => "user record created"
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user=User::find($id);
        return view('editprofile',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Updateuservalidate $request, $id)
    {
        $request->validated();

        $user=User::find($id);
        $user->update(['name' => $request->name,
        'username'=>$request->username,
        'email' => $request->email,
        'dob'=>$request->date,
                    ]);
        if ($request->profile_image){
            $image = $request->file('profile_image');
            $originalname = $request->username.'.jpg';
            $path = $image->storeAs('/images/profileimages',$originalname,'public');
            $user->update(['name' => $request->name,
                'username'=>$request->username,
                'email' => $request->email,
                'dob'=>$request->date,
                'profile_image' => $path,
            ]);
        }


        return redirect()->route('post.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function login(Request $request){
        $this->validate($request, [
            'email1' => 'required',
            'password1' => 'required'
        ]);

        if (Auth::attempt(['email' => $request['email1'], 'password' => $request['password1']])) {
            return redirect()->route('post.index');
        }
        return redirect()->back();
    }

    public function logout(Request $request){
        Auth::logout();
        return view('welcome');
    }
    public function data(){
        $user= Post::get()->toJson(JSON_PRETTY_PRINT);
        return response($user,200);
    }
}
