<?php

namespace App\Listeners;

use App\Events\AddEvent;
use App\Mail\Test;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class AddEventsend
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddEvent  $event
     * @return void
     */
    public function handle(AddEvent $event)
    {
        foreach ($event->user as $events)
        Mail::to($events->email)->send(
            new Test($events)
        );
    }
}
